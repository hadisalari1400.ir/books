import { NavLink, Outlet } from "react-router-dom";
import "./App.css";

const App = () => {
  return (
    <div className="App">
      <h1> کتابخانه من </h1>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          borderBottom: "1px solid black",
          margin: "1rem",
          padding: "1rem",
        }}
      >
        <NavLink
          style={({ isActive }) => {
            return {
              marginLeft: "2rem",
              color: isActive ? "red" : "block",
            };
          }}
          to="/books"
        >
          کتاب ها
        </NavLink>
        <NavLink
          to="/about"
          style={({ isActive }) =>
            isActive ? { color: "red" } : { color: "black" }
          }
        >
          درباره ما
        </NavLink>
      </div>
      <Outlet />
    </div>
  );
};

export default App;
