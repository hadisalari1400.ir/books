import {
  NavLink,
  Outlet,
  useSearchParams,
  useLocation,
} from "react-router-dom";
import { getBooks } from "../data/data";

const Books = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const location = useLocation();

  const books = getBooks();
  return (
    <main
      style={{
        display: "flex",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          justifyContent: "center",
          borderLeft: "1px solid black",
          padding: "0 .8rem",
          marginLeft: "1rem",
        }}
      >
        <input
          type="text"
          placeholder="سرچ کتاب"
          value={searchParams.get("filter") || ""}
          onChange={(e) => {
            let filter = e.target.value;
            filter ? setSearchParams({ filter }) : setSearchParams({});
          }}
        />
        {books
          .filter((book) => {
            let filter = searchParams.get("filter");
            if (!filter) return true;
            let name = book.name.toLowerCase();

            return name.startsWith(filter.toLowerCase());
          })
          .map((book) => (
            <NavLink
              to={`/books/${book.number}${location.search}`}
              style={({ isActive }) =>
                isActive ? { color: "red" } : { color: "black" }
              }
              key={book.number}
            >
              {book.name}
            </NavLink>
          ))}
      </div>
      <Outlet />
    </main>
  );
};

export default Books;
