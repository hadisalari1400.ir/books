import { useParams, useNavigate, useLocation } from "react-router-dom";
import { getBook, deleteBook } from "../data/data";

const Book = () => {
  const params = useParams();
  const book = getBook(parseInt(params.bookID));
  const navigate = useNavigate();
  const location = useLocation();

  return (
    <main>
      {book ? (
        <>
          <h2> قیمت کل : {book.amount} </h2>
          <p> {book.name} </p>
          <p>تاریخ انتشار : {book.due}</p>
          <button
            onClick={() => {
              deleteBook(book.number);
              navigate("/books" + location.search);
            }}
          >
            حذف
          </button>
        </>
      ) : (
        <h2>چنین کتابی نداریم دوست من</h2>
      )}
    </main>
  );
};

export default Book;
